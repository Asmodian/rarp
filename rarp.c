#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <asm/types.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>

#define BUF_SIZE 42
/*EtherType это двухбайтовое поле в заголовке ethernet кадра,
 * которое содержит информацию о типе протокола инкапсулированных в данный кадр данных.
 * 0x8035 соответствует RARP протоколу*/
/*#define ETH_RARP 0x8035*/
#define DEVICE "wlp4s0"
#define ETH_MAC_LEN 6
#define BUF 1024
#define ARPFILE "/proc/net/arp"

struct __attribute__((packed)) arp_header
{
    unsigned short arp_hd;
    unsigned short arp_pr;
    unsigned char arp_hdl;
    unsigned char arp_prl;
    unsigned short arp_op;
    unsigned char arp_sha[6];
    unsigned char arp_spa[4];
    unsigned char arp_dha[6];
    unsigned char arp_dpa[4];
};

int getIP(unsigned char* mac, unsigned char* ip);

int main(void)
{
	int i, s; /*s - сокет*/
	int ifindex = 0; /*Индекс ethernet интерфейса*/
	char* buffer; /*буффер для ethernet кадра*/
	buffer = (char *)malloc(BUF_SIZE);
	struct ethhdr *eh = (struct ethhdr *)buffer;
	struct arp_header *ah = (struct arp_header *)(buffer + 14);
	
	struct ifreq ifr;
    struct sockaddr_ll socket_address;
    unsigned char src_mac[6]; /*наш mac-адрес*/
    
    printf("Server started, entering initialiation phase...\n");
    
    /*Открытие сокета*/
    s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (s == -1) {
        perror("socket:");
        return -1;
    }
    printf("Successfully opened socket: %i\n", s);
    
    /*Извлечение индекса ethernet интерфейса*/
    strncpy(ifr.ifr_name, DEVICE, IFNAMSIZ);
    if (ioctl(s, SIOCGIFINDEX, &ifr) == -1) {
        perror("SIOCGIFINDEX");
        return -1;
    }
    ifindex = ifr.ifr_ifindex;
    printf("Successfully got interface index: %i\n", ifindex);
    
    /*Извлечение mac-адреса нашего интерфейса*/
    if (ioctl(s, SIOCGIFHWADDR, &ifr) == -1) {
        perror("SIOCGIFINDEX");
        return -1;
    }
    for (i = 0; i < 6; i++) {
        src_mac[i] = ifr.ifr_hwaddr.sa_data[i];
    }
    printf("Successfully got our MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n",
        src_mac[0],src_mac[1],src_mac[2],src_mac[3],src_mac[4],src_mac[5]);
        
    /*подготовка sockaddr_ll*/
    socket_address.sll_family = PF_PACKET;
    socket_address.sll_protocol = htons(ETH_P_IP);
    socket_address.sll_ifindex = ifindex;
    socket_address.sll_hatype = ARPHRD_ETHER;
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    socket_address.sll_halen = 0;
    socket_address.sll_addr[6] = 0x00;
    socket_address.sll_addr[7] = 0x00;
    
    while(1)
    {
		if(recvfrom(s, buffer, BUF_SIZE, 0, NULL, NULL) < 0)
        {
            perror("recvfrom:");
            return -1;
        }
        
        if((htons(eh->h_proto) == ETH_P_RARP) || (htons(eh->h_proto) == ETH_P_ARP))
        {
			if(htons(ah->arp_op) != 0x0003)
                continue;
                
            printf("New request from mac-address: %02X:%02X:%02X:%02X:%02X:%02X\n",
                ah->arp_sha[0], ah->arp_sha[1], ah->arp_sha[2],
                ah->arp_sha[3], ah->arp_sha[4], ah->arp_sha[5]);
            
			
            for(i = 0; i < 6; i++)
			{
				ah->arp_sha[i] = src_mac[i];
				eh->h_dest[i] = eh->h_source[i];
				eh->h_source[i] = src_mac[i];
			}
			
			//здесь должен быть нормальный алгоритм записи ip из файла
			getIP(ah->arp_dha, ah->arp_dpa);
			/*for(i = 0; i < 4; i++)
			{
				ah->arp_dpa[i] = ah->arp_spa[i];
			}*/

			eh->h_proto = ntohs(ETH_P_RARP);
			ah->arp_hd = 0x0001;
			ah->arp_pr = 0x0800;
			ah->arp_hd = ntohs(ah->arp_hd);
            ah->arp_pr = ntohs(ah->arp_pr);
			ah->arp_op = ntohs(0x0004);
			
			if(sendto(s, buffer, BUF_SIZE, 0, (struct sockaddr*)&socket_address, sizeof(socket_address)) < 0)
			{
				perror("sendto:");
                return -1;
			}
            printf("Answer for request: %02d.%02d.%02d.%02d\n", ah->arp_dpa[0], ah->arp_dpa[1], ah->arp_dpa[2], ah->arp_dpa[3]);
		}
	}
}

int getIP(unsigned char* mac, unsigned char* ip)
{
	FILE *file;
	char str[BUF], macF[17], ipF[15];
	unsigned char macNumber[6];
	char *estr, *word, *last;
	int i = 0;
	file = fopen(ARPFILE, "r");
	estr = fgets(str,sizeof(str),file);
	while(estr != NULL)
	{
		estr = fgets(str,sizeof(str),file);
		
		word = strtok_r(str, " ", &last);
		while(word != NULL)
		{	
			if(i == 0)
				strcpy(ipF, word);
			if(i == 3)
				strcpy(macF, word);			
			word = strtok_r(NULL, " ", &last);
			i++;
		}
		i = 0;
		
		word = strtok_r(macF, ":", &last);
		while(word != NULL)
		{
			macNumber[i] = (unsigned char)strtol(word, NULL, 16);		
			word = strtok_r(NULL, ":", &last);
			i++;
		}
		i = 0;
		
		if(!strncmp(mac, macNumber, 6))
		{
			word = strtok_r(ipF, ".", &last);
			while(word != NULL)
			{
				ip[i] = (unsigned char)strtol(word, NULL, 10);		
				word = strtok_r(NULL, ".", &last);
				i++;
			}
		}
		
	}
	
}
